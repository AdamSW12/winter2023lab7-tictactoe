public enum Square{
		X,O,BLANK;
		
		public String toString(){
			if(this == Square.X){
				return "X";
			}
			if(this == Square.O){
				return "O";
			}
			if(this == Square.BLANK){
				return "-";
			}
		return " ";
		}
	}
