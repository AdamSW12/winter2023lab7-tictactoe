import java.util.Scanner;
public class TicTacToeGame{
	
	public static void main(String[] args){
		System.out.println("---------Welcome To TicTacToe---------\n ---------Thanks for playing---------");
		Scanner scan = new Scanner(System.in);
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		int row = 0;
		int col = 0;
		Square playerToken = Square.X;
		
		while(!(gameOver)){
			System.out.println(); //line for visual purposes
			System.out.print(board);
			
			if(player == 1){
				playerToken = Square.X;
			}
			else{
				playerToken = Square.O;
			}
			System.out.println("Player " + player + " which row do you want to place your " + playerToken + " in?");
			row = scan.nextInt() - 1;
			
			System.out.println("Which column do you want to place your " + playerToken + " in?");
			col = scan.nextInt() - 1;
			
			while(!(board.placeToken(row,col,playerToken))){
				System.out.println("!!Invalid token placement!! Please enter a new row and column");
				row = scan.nextInt() - 1;
				col = scan.nextInt() - 1;
				
			}
			System.out.println(playerToken + " Placed at " + row +"," + col);
			
			if(board.checkIfFull()){
				System.out.print(board);
				gameOver = true;
				System.out.println("Its a tie!");
			}
			else if(board.checkIfWinning(playerToken)){
				System.out.print(board);
				gameOver = true;
				System.out.println("Player " + player + " wins" );
				
			}
			else{
				player++;
				if(player > 2){
					player = 1;
				}
			}
		}
		
	}
}