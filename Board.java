public class Board{
	public Square[][] tictactoeBoard;
	
	 public Board(){
		tictactoeBoard = new Square[3][3];
		for(int i = 0; i < this.tictactoeBoard.length; i++){
			for(int j = 0; j<this.tictactoeBoard[i].length; j++){
				this.tictactoeBoard[i][j]=Square.BLANK;
			}
		}
	}
	
	public boolean placeToken(int row, int col, Square token){
		
		if(row>2 || col>2){
			return false;
		}
		if(this.tictactoeBoard[row][col] == Square.BLANK){
			this.tictactoeBoard[row][col]=token;
			return true;
		}
		else{
			return false;
		}
	}
	
	public boolean checkIfFull(){
		boolean full = true;
		for(int i = 0; i < this.tictactoeBoard.length; i++){
			for(int j = 0; j<this.tictactoeBoard[i].length; j++){
				if(this.tictactoeBoard[i][j]==Square.BLANK){
					full = false;
					break;
				}
			}
		}
		return full;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		boolean winningHorizontal = false;
		for(int i = 0; i < this.tictactoeBoard.length; i++){
			if(this.tictactoeBoard[i][0] == playerToken){
				if(this.tictactoeBoard[i][1] == playerToken && this.tictactoeBoard[i][2] == playerToken){
					winningHorizontal = true;
					break;
					//if a row is full of the same symbol, evaluates to a win
				}
				else{
					winningHorizontal = false;
				}
			}
			else{
				winningHorizontal = false;
			}
		}
		return winningHorizontal;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
		boolean winningVertical = false;
		for(int i = 0; i < this.tictactoeBoard.length; i++){
			if(this.tictactoeBoard[0][i] == playerToken){
				if(this.tictactoeBoard[1][i] == playerToken && this.tictactoeBoard[2][i] == playerToken){
					winningVertical = true;
					break;
					//if a column is full of the same symbol, evaluates to a win
				}
				else{
					winningVertical = false;
				}
			}
			else{
				winningVertical = false;
			}
		}
		return winningVertical;
	}
	
	public boolean checkIfWinning(Square playerToken){
		boolean winning = false;
		
		if(this.checkIfWinningVertical(playerToken)){
			winning = true;
		}
		if(this.checkIfWinningHorizontal(playerToken)){
			winning = true;
		}
		return winning;
	}
	public String toString(){
		String board = "";
		board += "  1 2 3 \n";
		for(int i = 0; i<this.tictactoeBoard.length;i++){
			board += (i + 1 + " ");
			for(int j=0; j<this.tictactoeBoard[i].length;j++){
				board += ( this.tictactoeBoard[i][j]+" ");
			}
			board += ("\n");
		}
		return board;
	}
	
}